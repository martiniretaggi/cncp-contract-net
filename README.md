# README #

Questo progetto � stato realizzato come parte integrante del corso di Sistemi Operativi 2 della Laurea Magistrale in Ingegneria Informatica e dell'Automazione presso l'UNIVPM, tenuto dal prof. A.F. Dragoni nell'A.A. 2016/2017. 
� stato sviluppato un sistema multi-agente tramite il framework JADE e l'IDE Eclipse, in particolare � stata implementata l'estensione CNCP del protocollo FIPA Contract Net.

Per maggiori dettagli, consultare il seguente documento: [CNCP Contract Net](https://bitbucket.org/martiniretaggi/cncp-contract-net/raw/39da298ac8ed7071064fd0ba58b0c7e5122609c4/Relazione-SO2-CNCP.pdf)

### REFERENZE ###


* FIPA Contract Net Interaction Protocol Specification, http://www.fipa.org/specs/fipa00029/SC00029H.html
* Jade Site | Java Agent DEvelopment Framework, http://jade.tilab.com/
* T. Knabe, M. Schillo, K. Fischer, Improvements to the fipa contract net protocol for performance increase and cascading applications (2002).


### RELATORI ###

* Diego Retaggi
* Massimo Martini