package agents;


import java.util.Random;
import java.util.concurrent.TimeUnit;

import behav.FSMParticipant;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;

public class Participant extends Agent{
	protected void setup() {
		System.out.println(this.getLocalName() + ": inizializzazione...");
		
		//leggo il primo argomento dalla console, che dovr� essere il costo utilizzato dal Participant per eseguire un task
		int val=0;
		Object[] args=this.getArguments();
		if(args!=null){
			val=Integer.parseInt((String)args[0]);
		}
		
		//Inizializzo FSMBehaviour del Participant
		FSMP fsmp=new FSMP(this,val);
		this.addBehaviour(fsmp);
	}
	
	
	//Operazioni di clean-up
	protected void takeDown(){
		//Deregistrazione dal df
		try {
			DFService.deregister(this);
		} catch (FIPAException e) {
			e.printStackTrace();
		}
	}
	
	class FSMP extends FSMParticipant{

		public FSMP(Agent a, int val) {
			super(a, val);
			// TODO Auto-generated constructor stub
		}
		
		//sovrascrivo le funzioni replyCFP(), replyRequest(), doTask(), per poter personalizzare il Participant in base alle mie esigenze
		//(in realt� qui ho riscritto le stesse istruzioni che si trovano nel FSMParticipant)
		
		//funzione per rispondere ad una CFP --- true=PROPOSE	false=REFUSE
		public boolean replyCFP(){
			return !busy;
		}
		//funzione per rispondere ad una REQUEST --- true=AGREE	false=REFUSE
		public boolean replyRequest(){
			return !busy;
		}
		//funzione che serve per eseguire effettivamente il task, poi invia come risposta un vettore di due object: il primo elemento riguarda la permormative da inviare,
		//il secondo invece riguarda la stringa da mettere nel Content del messaggio se abbiamo avuto una INFORM_REF
		public Object[] doTask(){
			try {
				TimeUnit.SECONDS.sleep(7);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			Random random = new Random();
			int casuale=random.nextInt(3)+1;
			Object[] risp=new Object[2];
			
			switch(casuale){
			case 1:
				risp[0]=ACLMessage.FAILURE;
				break;
			case 2:
				risp[0]=ACLMessage.INFORM;
				break;
			case 3:
				risp[0]=ACLMessage.INFORM_REF;
				risp[1]=Integer.toString(casuale);
				break;
			}
			return risp;
		}
	}
}
