package agents;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import behav.FSMInitiator;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

public class Initiator extends Agent{
	protected void setup() {
		System.out.println(this.getLocalName() + ": inizializzazione...");
		//Faccio aspettare gli Iniziator per 2 secondi, in modo tale da dare il tempo ai Participant di iscriversi al DF
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//Inizializzo FSMBehaviour dell'Initiator
		FSMI fsmi=new FSMI(this);
		fsmi.setReboot(true);	//Se il Participant fallisce il task, l'Initiator torner� al suo stato iniziale e ricercher� nuovi Participant
		fsmi.setSearchPeriod(5000);	//Periodo con cui viene effettuata una nuova ricerca dei Participant nel DF
		fsmi.setDead1(5000);	//deadline delle CFP
		fsmi.setDead2(5000);	//deadline delle REQUEST
		this.addBehaviour(fsmi);
	}
	
	class FSMI extends FSMInitiator{

		public FSMI(Agent a) {
			super(a);
			// TODO Auto-generated constructor stub
		}
		
		//Sovrascrivo il metodo sortProposal per personalizzare il criterio con cui vado a scegliere il miglior offerente.
		//Utilizzo la getList() e la setList() per prendere e modificare il vettore dei messaggi PROPOSE dei Participant ricevuti dall'Initiator.
		
		//In questo esempio ordino i messaggi dal pi� costoso al meno costoso, mentre nella sortProposal standard facevo il contrario
		public void sortProposal(){
			
			ArrayList<ACLMessage> agenti=this.getList();
			ArrayList<ACLMessage> app=new ArrayList<ACLMessage>();
			int size=agenti.size();
			for(int i=0;i<size;i++){
				ACLMessage max=null;
				for(int j=0;j<agenti.size();j++){
					if(max==null)	max=agenti.get(j);
					else if(Integer.parseInt(agenti.get(j).getContent())>Integer.parseInt(max.getContent())){
						max=agenti.get(j);
					}
				}
				app.add(max);
				agenti.remove(max);
			}
			this.setList(app); 
		} 
	}
	
}
