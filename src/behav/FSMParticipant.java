package behav;


import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class FSMParticipant extends FSMBehaviour{
	private static long tempo;
	//funzione che stampa il tempo passato dall'inizio dell'esecuzione in poi (per il debug)
	public static String time(){
		return "----------------"+(now()-tempo);
	}
	// nomi degli STATI
	private static final String STATE_A = "A";
	private static final String STATE_B = "B";
	private static final String STATE_C = "C";
	private static final String STATE_D = "D";
	//gli STATI
	StateA sa;
	StateB sb;
	StateC sc;
	StateD sd;

	private boolean reboot=true;	//TRUE->Se il Participant ha finito il task, allora lo faccio ricominciare dallo stato iniziale
	
	private int val;	//Costo del Participant per svolgere un task
	
	protected boolean busy=false;	//serve per capire quando il Participant sta svolgendo un task, e quindi non pu� rispondere ad altri Initiator (TRUE->sta svolgendo un task)
	private int cont=0;	//contatore per sapere a quante CFP sto rispondendo
	private AID boss=null;	//Informazioni riguardanti l'Initiator a cui questo Participant ha mandato AGREE
	
	public FSMParticipant(Agent a, int val){
		super(a);
		this.val=val;	//rappresenta il costo del Participant per eseguire un task
		tempo=now();
		// Registro lo stato A (stato iniziale)
		sa=new StateA(myAgent);
		registerFirstState(sa, STATE_A);
		
		// Registro lo stato B
		sb=new StateB(myAgent);
		registerState(sb, STATE_B);
		
		// Registro lo stato C
		sc=new StateC(myAgent);
		registerState(sc, STATE_C);
		
		// Registro lo stato D (stato finale)
		sd=new StateD(myAgent);
		registerLastState(sd, STATE_D);

		// Registro le transizioni
		registerTransition(STATE_A, STATE_A, 0);
		registerTransition(STATE_A, STATE_B, 1);
		registerTransition(STATE_B, STATE_A, 0);
		registerTransition(STATE_B, STATE_C, 1);
		registerTransition(STATE_B, STATE_B, 2);
		registerTransition(STATE_C, STATE_A, 0);
		registerTransition(STATE_C, STATE_D, 1);
		registerTransition(STATE_C, STATE_B, 2);
		
	}
	
	//FSMParticipant completato
	public int onEnd() {
		System.out.println("FSMParticipant behaviour completed."+time());
		myAgent.doDelete();
		return super.onEnd();
	}
	//leggo l'orario per calcolare le deadline
	public static long now(){
		return new Date().getTime();
	}
	//funzione per rispondere ad una CFP --- true=PROPOSE	false=REFUSE
	public boolean replyCFP(){
		return !busy;
	}
	//funzione per rispondere ad una REQUEST --- true=AGREE	false=REFUSE
	public boolean replyRequest(){
		return !busy;
	}
	//quando mi arriva una CFP controllo cosa deve risponder il Participant utilizzando la funzione replyCFP()
	public int checkCFP(ACLMessage msg){
		int risp=0;
		System.out.println(myAgent.getLocalName() + ":  mi � arrivata una CFP da "+msg.getSender().getLocalName()+time());
		
		ACLMessage reply = msg.createReply();
		
		if(replyCFP()){
			reply.setPerformative(ACLMessage.PROPOSE);
			System.out.println(myAgent.getLocalName() + ": ho mandato una PROPOSE a "+msg.getSender().getLocalName()+time());
			cont++;
			risp=1;
		}else{
			reply.setPerformative(ACLMessage.REFUSE);
			System.out.println(myAgent.getLocalName() + ": ho mandato una REFUSE a "+msg.getSender().getLocalName()+time());
		}
		
		
		reply.setContent(Integer.toString(val));
		//prima di rispondere alla CFP, controllo se ho sforato la sua deadline1
		if(now()>msg.getReplyByDate().getTime()){
			//ho sforato la deadline1 e quindi non mando pi� il messaggio
			System.out.println(myAgent.getLocalName() + ": DEADLINE scaduta della CFP ricevuta da "+msg.getSender().getLocalName()+time());
			risp=0;
			if(reply.getPerformative()==ACLMessage.PROPOSE)	cont--;
		}else{
			myAgent.send(reply);
		}
		return risp;
	}
	//setto la variabile reboot
	public void setReboot(boolean boot){
		reboot=boot;
	}
	//funzione che simula il task e invia come risposta un vettore di due object: il primo elemento riguarda la permormative da inviare,
	//il secondo invece riguarda la stringa da mettere nel Content del messaggio se abbiamo avuto una INFORM_REF
	public Object[] doTask(){
		try {
			TimeUnit.SECONDS.sleep(7);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		Random random = new Random();
		int casuale=random.nextInt(3)+1;
		Object[] risp=new Object[2];
		
		switch(casuale){
		case 1:
			risp[0]=ACLMessage.FAILURE;
			break;
		case 2:
			risp[0]=ACLMessage.INFORM;
			break;
		case 3:
			risp[0]=ACLMessage.INFORM_REF;
			risp[1]=Integer.toString(casuale);
			break;
		}
		return risp;
	}
	
	private class StateA extends TickerBehaviour{
		private int risp=0;	//valore di ritorno per eseguire le transizioni dello stato
		public StateA(Agent a){
			super(a,1);
			//Registro il Participant nel DF
			DFAgentDescription ad = this.getAgentDescription();
			try {
				System.out.println(myAgent.getLocalName() + ": mi registro nel DF"+time());
				DFService.register(myAgent, ad);
			} catch (FIPAException e) {
				e.printStackTrace();
			}
		}
		// Creo il servizio del Participant
		public DFAgentDescription getAgentDescription()	{
			DFAgentDescription ad = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setName("Servizio");
			sd.setType("Servizio");
			ad.addServices(sd);
			return ad;
		}
		public void onStart(){
			System.out.println("(A)-"+myAgent.getLocalName());
			cont=0;
			risp=0;
			
		}
		

		@Override
		public void onTick() {
			// Ricezione
			ACLMessage msg = myAgent.receive();
			
			// Se c'e' un messaggio
			if (msg != null){
				if(msg.getPerformative()==ACLMessage.CFP){
					//rispondo alla CFP
					risp=checkCFP(msg);
					this.stop();
				}
			}
			// Se non c'e' un messaggio
			else{
				// Il behaviour si sospende fino alla ricezione di nuovi messaggi
				this.block();
			}
		}
		public int onEnd(){
			if(risp==0)	sa.reset();
			else{
				if(sb.done())	sb.reset();
			}
			return risp;
		}
	}
	private class StateB extends TickerBehaviour{
		private int risp=0;	//valore di ritorno per eseguire le transizioni dello stato
		public StateB(Agent a) {
			super(a, 1);
		}
		public void onStart(){
			System.out.println("(B)-"+myAgent.getLocalName());
			risp=0;
		}
		@Override
		public void onTick() {
			// Ricezione
			ACLMessage msg = myAgent.receive();
			
			// Se c'e' un messaggio
			if (msg != null){
				//controllo se � un messaggio CFP, REQUEST oppure REJECT_PROPOSAL
				if(msg.getPerformative()==ACLMessage.CFP){
					//rispondo alla CFP
					checkCFP(msg);
					risp=2;
					this.stop();
				}else if(msg.getPerformative()==ACLMessage.REQUEST){
					System.out.println(myAgent.getLocalName() + ": mi � arrivata una REQUEST da "+msg.getSender().getLocalName()+time());
					//rispondo alla Request, solo se ho rispettato la sua deadline2
					if(now()<=msg.getReplyByDate().getTime()){
						cont--;
						ACLMessage reply = msg.createReply();
						if(replyRequest()){
							reply.setPerformative(ACLMessage.AGREE);
							risp=1;
							myAgent.send(reply);
							System.out.println(myAgent.getLocalName() + ": ho mandato una AGREE a "+msg.getSender().getLocalName()+time());
							//se ho mandato AGREE, allora setto la variabile busy a TRUE e mi salvo le informazioni dell'Initiator
							busy=true;
							boss=msg.getSender();
							this.stop();
						}
						else{
							reply.setPerformative(ACLMessage.REFUSE);
							myAgent.send(reply);
							System.out.println(myAgent.getLocalName() + ": ho mandato una REFUSE a "+msg.getSender().getLocalName()+time());
							//se rispondo REFUSE, devo capire se tornare allo stato iniziale oppure rimanere qui. Dipende dal valore di cont
							if(cont==0)		risp=0;
							else 			risp=2;
							this.stop();
						}
					}else{
						System.out.println(myAgent.getLocalName() + ": DEADLINE2 scaduta della REQUEST ricevuta da "+msg.getSender().getLocalName()+time());
					}
					
				}else if(msg.getPerformative()==ACLMessage.REJECT_PROPOSAL){
					//ricevuta una REJECT_PROPOSAL, devo capire se tornare allo stato iniziale oppure rimanere qui. Dipende dal valore di cont
					System.out.println(myAgent.getLocalName() + ": mi � arrivata una REJECT_PROPOSAL da "+msg.getSender().getLocalName()+time());
					cont--;
					if(cont==0)		risp=0;
					else 			risp=2;
					this.stop();
				}
				
			}
			// Se non c'e' un messaggio
			else{
				// Il behaviour si sospende fino alla ricezione di nuovi messaggi
				this.block();
			}
		}
		public int onEnd(){
			if(risp==0)	sa.reset();
			else if(risp==1)	sc.reset();
			else if(risp==2){
				if(sb.done())	sb.reset();
			}
			return risp;
		}
	}
	private class StateC extends TickerBehaviour{
		private int risp=0;	//valore di ritorno per eseguire le transizioni dello stato
		public StateC(Agent a) {
			super(a, 1);
		}
		public void onStart(){
			System.out.println("(C)-"+myAgent.getLocalName());
			risp=0;
		}
		@Override
		public void onTick() {
			// Ricezione
			ACLMessage msg = myAgent.receive();
			// Se c'e' un messaggio
			if (msg != null){
				//Controllo se il messaggio arrivato proviene dall'Initiator a cui ho mandato l'AGREE
				if(msg.getSender().getLocalName().equals(boss.getLocalName())){
					if(msg.getPerformative()==ACLMessage.ACCEPT_PROPOSAL){
						System.out.println(myAgent.getLocalName() + ": Inizio il lavoro!!"+time());
						//Eseguo il taske mi ritorna l'esito
						Object[] out=doTask();
						//Rispondo in base all'esito del task
						ACLMessage reply = msg.createReply();
						switch((int)out[0]){
						case ACLMessage.FAILURE:
							System.out.println(myAgent.getLocalName() + ": FAILURE!!"+time());
							reply.setPerformative(ACLMessage.FAILURE);
							break;
						case ACLMessage.INFORM:
							System.out.println(myAgent.getLocalName() + ": INFORM!!"+time());
							reply.setPerformative(ACLMessage.INFORM);
							break;
						case ACLMessage.INFORM_REF:
							System.out.println(myAgent.getLocalName() + ": INFORM_REF!!"+time());
							reply.setPerformative(ACLMessage.INFORM_REF);
							reply.setContent((String)out[1]);
							break;
						}
						

						myAgent.send(reply);
						if(reboot){
							if(cont==0)	risp=0;
							else 		risp=2;
						}else{
							risp=1;
						}
						this.stop();
					}else if(msg.getPerformative()==ACLMessage.REJECT_PROPOSAL){
						//ho ricevuto una REJECT_PROPOSAL perch� prima avevo dato AGREE ma � scaduta la dead2 durante l'invio della AGREE, a causa di ritardi nella rete
						cont--;
						if(cont==0)	risp=0;
						else 		risp=2;
						this.stop();
					}
				}
			}
			// Se non c'e' un messaggio
			else{
				// Il behaviour si sospende fino alla ricezione di nuovi messaggi
				this.block();
			}
		}
		
		public int onEnd(){
			if(risp==0){
				busy=false;
				boss=null;
				sa.reset();
			}else if(risp==2){
				busy=false;
				boss=null;
				sb.reset();
			}
			return risp;
		}
	}
	private class StateD extends OneShotBehaviour{
		public StateD(Agent a) {
			super(a);
		}
		public void action(){
			System.out.println("(D)-"+myAgent.getLocalName());
		}
	}
}
