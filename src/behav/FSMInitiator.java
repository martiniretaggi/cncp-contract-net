package behav;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

public class FSMInitiator extends FSMBehaviour{
	//funzione che stampa il tempo passato dall'inizio dell'esecuzione in poi (per il debug)
	private static String time(){
		return FSMParticipant.time();
	}
	// nomi degli STATI dell FSMBehaviour
	private static final String STATE_A = "A";
	private static final String STATE_B = "B";
	private static final String STATE_C = "C";
	private static final String STATE_D = "D";
	private static final String STATE_E = "E";
	//gli STATI
	StateA sa;
	StateB sb;
	StateC sc;
	StateD sd;
	StateE se;
	
	private boolean reboot=true;	//TRUE->Se il Participant fallisce il task, l'Initiator torner� al suo stato iniziale e ricercher� nuovi Participant

	private static long searchPeriod=5000;	//periodo per la ricerca del servizio nel DF
	private static long dead1=5000;	//prima deadline per aspettare le risposte delle CFP dai Participant (deadline relativa)
	private static long dead2=5000;	//seconda deadline per aspettare le risposte delle Request dai participant (deadline relativa)
	
	private long timeDead1;	//dead1 come deadline assoluta
	private long timeDead2; //dead2 come deadline assoluta
	
	private AID acceptedAgent=null;		//informazioni riguardanti il Participant che ha accettato di svolgere il task
	private ArrayList<ACLMessage> agenti;	//array contentenente i messaggi di Propose dei Participant
	private int ncfp=0;		//numero di cfp che l'initiator ha inviato
	private DFAgentDescription[] ads;	//vettore contenente le informazioni di tutti gli agenti che offrono il servizio che ricerco
	
	
	public FSMInitiator(Agent a){
		super(a);
		
		// Registro lo stato A (stato iniziale)
		sa=new StateA(myAgent);
		registerFirstState(sa, STATE_A);
		
		// Registro lo stato B
		sb=new StateB(myAgent);
		registerState(sb, STATE_B);
		
		// Registro lo stato C
		sc=new StateC(myAgent);
		registerState(sc, STATE_C);
		
		// Registro lo stato D
		sd=new StateD(myAgent);
		registerState(sd, STATE_D);
		
		// Registro lo stato E (stato finale)
		se=new StateE(myAgent);
		registerLastState(se, STATE_E);
		
		// Registro le transizioni
		registerDefaultTransition(STATE_A, STATE_B);
		registerTransition(STATE_B, STATE_A, 0);
		registerTransition(STATE_B, STATE_C, 1);
		registerTransition(STATE_C, STATE_A, 0);
		registerTransition(STATE_C, STATE_D, 1);
		registerTransition(STATE_D, STATE_A, 0);
		registerTransition(STATE_D, STATE_E, 1);
	}
	
	//FSMInitiator completato
	public int onEnd() {
		System.out.println("FSMInitiator behaviour completed."+time());
		myAgent.doDelete();
		return super.onEnd();
	}
	
	//leggo l'orario per calcolare le deadline
	public long now(){
		return new Date().getTime();
	}
	//mi ritorna il vettore dei messaggi dei Participant che hanno mandato Propose
	public ArrayList<ACLMessage> getList(){
		return agenti;
	}
	//modifico il vettore dei messaggi dei Participant che hanno mandato Propose
	public void setList(ArrayList<ACLMessage> list){
		agenti=list;
	}
	//setto la variabile reboot
	public void setReboot(boolean boot){
		reboot=boot;
	}
	//setto la variabile searchPeriod
	public void setSearchPeriod(long time){
		searchPeriod=time;
	}
	//setto la variabile dead1
	public void setDead1(long time){
		dead1=time;
	}
	//setto la variabile dead2
	public void setDead2(long time){
		dead2=time;
	}
	//funzione per ordinare l'array dei messaggi di propose in ordine crescente rispetto al costo
	public void sortProposal(){
		ArrayList<ACLMessage> app=new ArrayList<ACLMessage>();
		int size=agenti.size();
		for(int i=0;i<size;i++){
			ACLMessage min=null;
			for(int j=0;j<agenti.size();j++){
				if(min==null)	min=agenti.get(j);
				else if(Integer.parseInt(agenti.get(j).getContent())<Integer.parseInt(min.getContent())){
					min=agenti.get(j);
				}
			}
			app.add(min);
			agenti.remove(min);
		}
		agenti=app;
	}
	
	private class StateA extends TickerBehaviour{
		
		public StateA(Agent a) {
			super(a, searchPeriod);
		}
		public void onStart(){
			System.out.println("(A)-"+myAgent.getLocalName()+time());
			ncfp=0;
			agenti=null;
			acceptedAgent=null;
			timeDead1=0;
			//per sicurezza svuoto la coda dei messaggi ricevuti
			while(myAgent.receive()!=null){
				
			}
		}
		protected void onTick(){
			try {
				//Ricerco il servizio sul DF
				System.out.println(this.myAgent.getLocalName() + ": Ricerco i servizi nel DF"+time());
				DFAgentDescription ad = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("Servizio");
				ad.addServices(sd);
				
				ads = DFService.search(this.myAgent, ad);
				//Se ho trovato qualche agente che offre il servizio che mi serve, allora mando loro le CFP		
				if (ads.length > 0)	{
					ncfp=ads.length;
					ACLMessage serviceRequest = new ACLMessage(ACLMessage.CFP);
					
					for (int i = 0; i < ads.length; i++){
						System.out.println(this.myAgent.getLocalName() + ": mando CFP a " + ads[i].getName().getLocalName()+time());
						serviceRequest.addReceiver(ads[i].getName());
					}
					timeDead1=now()+dead1;
					serviceRequest.setReplyByDate(new Date(timeDead1));				
					this.myAgent.send(serviceRequest);
					this.stop();
				}
				else{
					System.out.println(this.myAgent.getLocalName() + ": servizio non trovato"+time());
				}
			} catch (FIPAException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public int onEnd(){
			if(sb.done())	sb.reset();
			return 0;
		}
	}
	private class StateB extends TickerBehaviour{
		private int risp=0;			//valore di ritorno per effettuare le transizioni dello stato
		private long timeCreate;	//usato per calcolare la deadline1
		private int count=0; 		//contatore delle risposte alle CFP
		public StateB(Agent a) {
			super(a,1);
		}
		public void onStart(){
			System.out.println("(B)-"+myAgent.getLocalName()+time());
			count=0;
			risp=0;
			timeCreate=now();
			agenti=new ArrayList<ACLMessage>();
		}
		public void onTick(){
			//controllo se ho ricevuto tutte le risposte alle mie CFP
			if(count<ncfp){
				//controllo se ho sforato la deadline1
				if(now()<=timeDead1){
					// Ricezione
					ACLMessage msg = this.myAgent.receive();
					
					// Se c'e' un messaggio
					if (msg != null){
						if(msg.getPerformative()==ACLMessage.PROPOSE){
							risp=1;
							count++;
							System.out.println(this.myAgent.getLocalName() + ": "+msg.getSender().getLocalName()+" ha mandato una propose!"+time());
							agenti.add(msg);
						}
							
						else if(msg.getPerformative()==ACLMessage.REFUSE || msg.getPerformative()==ACLMessage.NOT_UNDERSTOOD){
							System.out.println(this.myAgent.getLocalName() + ": "+msg.getSender().getLocalName()+" ha rifiutato!"+time());
							count++;
						}
					}
					
				}else{
					//Deadline1 scaduta
					System.out.println(this.myAgent.getLocalName() + ": Deadline scaduta"+time());
					sendReject();
					checkTransition();
				}
			}else{
				//Mi hanno risposto tutti
				System.out.println(this.myAgent.getLocalName() + ": Ho ricevuto tutte le risposte"+time());
				checkTransition();
			}
		}
		//controllo quanti messaggi di propose ho e ordino il vettore
		public void checkTransition(){
			if(agenti.size()==0){
				risp=0;
				System.out.println(this.myAgent.getLocalName() + ": Non ho propose!"+time());
			}
			else{
				risp=1;
				//ordino l'array
				sortProposal();
			}
			this.stop();
		}
		//invio le REJECT_PROPOSAL a tutti i participant che non mi hanno risposto entro la dead1
		public void sendReject(){
			for(int i=0;i<ads.length;i++){
				boolean flag=false;
				for(int j=0;j<agenti.size();j++){
					if(agenti.get(j).getSender().getLocalName().equals(ads[i].getName().getLocalName())){
						flag=true;
						break;
					}
				}
				if(!flag){
					ACLMessage serviceReject = new ACLMessage(ACLMessage.REJECT_PROPOSAL);
					serviceReject.addReceiver(ads[i].getName());
					myAgent.send(serviceReject);
					System.out.println(this.myAgent.getLocalName() + ": "+"REJECT_PROPOSAL(dead1)-->"+ads[i].getName().getLocalName()+time());
				}
			}
		}
		public int onEnd(){
			if(risp==0)	sa.reset();
			else{
				if(sc.done())	sc.reset();
			}
			return risp;
		}
	}
	private class StateC extends TickerBehaviour{
		int i=0;						//indice per spostarmi sull'array "agenti", partendo dal miglior offerente
		private int risp=0;				//valore di ritorno per effettuare le transizioni dello stato
		private boolean exit=false;		//booleano che mi aiuta a terminare lo stato C
		
		public StateC(Agent a) {
			super(a,1);
		}
		public void onStart(){
			System.out.println("(C)-"+myAgent.getLocalName()+time());
			i=0;
			risp=0;
			exit=false;
			timeDead2=0;
		}
		@Override
		public void onTick() {
			//Scorro l'array agenti partendo dal migliore offerente
			if(agenti.size()>i){
				//invio la Request
				ACLMessage request = agenti.get(i).createReply();
				request.setPerformative(ACLMessage.REQUEST);
				timeDead2=now()+dead2;
				request.setReplyByDate(new Date(timeDead2));
				this.myAgent.send(request);
				System.out.println(this.myAgent.getLocalName() + ": "+"REQUEST-->"+agenti.get(i).getSender().getLocalName()+time());
				
				//Mi metto in ascolto per ricevere la risposta alla Request, rispettando per� la deadline2
				boolean received=false;
				while(now()<=timeDead2){
					ACLMessage msg = this.myAgent.receive();
					//Se c'� un mesasggio
					if (msg != null){
						//controllo che il messaggio sia arrivato dallo stesso partecipante a cui ho inviato la REQUEST
						if(msg.getSender().getLocalName().equals(agenti.get(i).getSender().getLocalName())){
							received=true;
							if(msg.getPerformative()==ACLMessage.AGREE){
								risp=1;
								acceptProposal();
								exit=true;
								break;	
							}else if(msg.getPerformative()==ACLMessage.REFUSE){
								break;
							}
						}						
					}
					
				}
				if(exit)	this.stop();	//se qualcuno ha risposto AGREE allora smetto di inviare Request e vado allo stato successivo
				
				if(!received){	//se non ho ricevuto risposta entro la deadline, gli mando una reject_proposal per sicurezza
					ACLMessage reject = new ACLMessage(ACLMessage.REJECT_PROPOSAL);
					reject.addReceiver(agenti.get(i).getSender());
					this.myAgent.send(reject);
					System.out.println(this.myAgent.getLocalName() + ": "+"REJECT_PROPOSAL(dead2)-->"+agenti.get(i).getSender().getLocalName()+time());
				}
				i++;
			}else{
				//ho finito l'array agenti
				System.out.println(this.myAgent.getLocalName() + ": nessun Participant mi fa il task"+time());
				risp=0;
				this.stop();
			}
		}
		public int onEnd(){
			if(risp==0)	sa.reset();
			else{
				if(sd.done())	sd.reset();
			}
			return risp;
		}
		//Cosa fare quando qualcuno ha risposto AGREE
		public void acceptProposal(){	
			//Mando tutte le REJECT-PROPOSAL ai Participant al di sotto di quello che mi ha accettato
			for(int j=i+1;j<agenti.size();j++){
				ACLMessage reject = agenti.get(j).createReply();
				reject.setPerformative(ACLMessage.REJECT_PROPOSAL);
				this.myAgent.send(reject);
				System.out.println(this.myAgent.getLocalName() + ": "+"REJECT_PROPOSAL-->"+agenti.get(j).getSender().getLocalName()+time());
			}
			//mando Accept-Proposal a quello che ha accettato
			ACLMessage ap = agenti.get(i).createReply();
			ap.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
			this.myAgent.send(ap);
			System.out.println(this.myAgent.getLocalName() + ": "+"ACCEPT_PROPOSAL-->"+agenti.get(i).getSender().getLocalName()+time());
			acceptedAgent=agenti.get(i).getSender();
		}
	}
	private class StateD extends TickerBehaviour{
		private int risp=0;	//valore di ritorno per eseguire le transizioni dello stato
		public StateD(Agent a) {
			super(a,1);
		}
		public void onStart(){
			System.out.println("(D)-"+myAgent.getLocalName()+time());
			risp=0;
		}
		@Override
		public void onTick() {
			// Ricezione
			ACLMessage msg = this.myAgent.receive();
			// Se c'e' un messaggio
			if (msg != null){
				//controllo che arriva dallo stesso agente che doveva fare il task
				if(msg.getSender().getLocalName().equals(acceptedAgent.getLocalName())){
					if(msg.getPerformative()==ACLMessage.INFORM){
						risp=1;
						System.out.println(this.myAgent.getLocalName() + ": "+msg.getSender().getLocalName()+" mi ha mandato una INFORM!"+time());
						this.stop();
					}else if(msg.getPerformative()==ACLMessage.INFORM_REF){
						risp=1;
						System.out.println(this.myAgent.getLocalName() + ": "+msg.getSender().getLocalName()+" mi ha mandato una INFORM_REF con content="+msg.getContent()+time());
						this.stop();
					}else if(msg.getPerformative()==ACLMessage.FAILURE){
						if(reboot)	risp=0;
						else	risp=1;
						System.out.println(this.myAgent.getLocalName() + ": "+msg.getSender().getLocalName()+" mi ha mandato una FAILURE!"+time());
						this.stop();
					}
				}
			}
			else	this.block();
		}
		public int onEnd(){
			if(risp==0){
				sa.reset();
			}
			return risp;
		}
	}
	
	private class StateE extends OneShotBehaviour{
		public StateE(Agent a) {
			super(a);
		}
		public void action(){
			System.out.println("(E)-"+myAgent.getLocalName());
		}
	}
}
